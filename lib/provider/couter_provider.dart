import 'package:flutter/foundation.dart';

class CounterProvider with ChangeNotifier, DiagnosticableTreeMixin {
  int _counter = 0;
  int get getCounterValue {
    return _counter;
  }

  void counterUp() {
    _counter++;
    notifyListeners();
  }

  void counterDown() {
    _counter--;
    notifyListeners();
  }
}
