import 'package:flutter/material.dart';
import 'package:flutter_provider/provider/couter_provider.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('Hello from build Widget');
    return Scaffold(
      appBar: AppBar(
        title: Text(DateTime.now().second.toString()),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const _buildCounterDown(),
            const _buildShowCounter(),
            const _buildCounterUp(),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, '/second'),
                child: const Text('Second Screen')),
          ],
        ),
      ),
    );
  }
}

class _buildShowCounter extends StatelessWidget {
  const _buildShowCounter({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      context.watch<CounterProvider>().getCounterValue.toString(),
      style: const TextStyle(fontSize: 50),
    );
  }
}

class _buildCounterUp extends StatelessWidget {
  const _buildCounterUp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: () => context.read<CounterProvider>().counterUp(),
        child: const Text('Counter+'));
  }
}

class _buildCounterDown extends StatelessWidget {
  const _buildCounterDown({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: () => context.read<CounterProvider>().counterDown(),
        child: const Text('Counter-'));
  }
}
