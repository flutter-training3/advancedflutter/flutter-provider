import 'package:flutter/material.dart';
import 'package:flutter_provider/provider/couter_provider.dart';
import 'package:provider/provider.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('Hello from second screen');
    return Scaffold(
      appBar: AppBar(
        title: Text('Secound ${DateTime.now().second}'),
        actions: [
          Consumer<CounterProvider>(
            builder: (context, counter, child) => IconButton(
                onPressed: () => counter.counterUp(),
                icon: const Icon(Icons.add)),
          ),
          Consumer<CounterProvider>(
            builder: (context, counter, child) => IconButton(
                onPressed: () => counter.counterDown(),
                icon: const Icon(Icons.remove)),
          )
        ],
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text('Counter : '),
          Consumer<CounterProvider>(
            builder: (context, counter, child) => Text(
              '${counter.getCounterValue}',
              style: const TextStyle(fontSize: 40),
            ),
          ),
          ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Home Screen')),
        ],
      )),
    );
  }
}
