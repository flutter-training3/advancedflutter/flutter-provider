import 'package:flutter/material.dart';
import 'package:flutter_provider/home_screen.dart';
import 'package:flutter_provider/provider/couter_provider.dart';
import 'package:flutter_provider/second_screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => CounterProvider(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Provider',
        theme: ThemeData(primarySwatch: Colors.red),
        // home: const HomeScreen(),
        initialRoute: '/home',
        routes: {
          '/home': (context) => const HomeScreen(),
          '/second': (context) => const SecondScreen()
        },
      ),
    );
  }
}
